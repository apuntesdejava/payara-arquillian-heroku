package com.apuntesdejava.arquillian.rest;

import com.apuntesdejava.arquillian.domain.Product;
import com.apuntesdejava.arquillian.logic.ProductFacade;
import java.util.List;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Diego Silva <diego.silva@apuntesdejava.com>
 */
@Path("product")
@Produces(MediaType.APPLICATION_JSON)
@Singleton
public class ProductREST {

    @Inject
    private ProductFacade facade; //instanciamos el EJB

    @GET
    public List<Product> findByName(
            @QueryParam("name") @DefaultValue("") String name
    ) {
        return facade.findByName(name); //solo llamamos al método del EJB
    }
}
